using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication3.Models;

namespace WebApplication3.Controllers
{
    public class FlightController : ApiController
    {
        public static List<Flight> flights = new List<Flight>();

        static FlightController()
        {
            /*
            flights.Add(new Flight
            {
                Id = 1,
                OriginCountry = "Israel",
                DestCountry = "Istanbul",
                Remaining = 270
            });
            flights.Add(new Flight
            {
                Id = 2,
                OriginCountry = "USA",
                DestCountry = "Japan",
                Remaining = 3
            });
            flights.Add(new Flight
            {
                Id = 3,
                OriginCountry = "Denemark",
                DestCountry = "Sweden",
                Remaining = 482
            });
            flights.Add(new Flight
            {
                Id = 4,
                OriginCountry = "Korea",
                DestCountry = "Taiwan",
                Remaining = 12
            });
            */
        }

        // GET api/flights all flights
        public IEnumerable<Flight> Get()
        {
            List<Flight> real_flights = new List<Flight>();

            using (SQLiteConnection conn = new SQLiteConnection("Data Source = D:\\SQLITE\\0905.db; Version = 3;"))
            {
                conn.Open();

                using (SQLiteCommand select_query = new SQLiteCommand("SELECT * from Flights", conn))
                {
                    using (SQLiteDataReader reader = select_query.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            real_flights.Add(
                                new Flight
                                {
                                    Id = (long)reader["ID"],
                                    DestCountry = reader["DestCountry"].ToString(),
                                    OriginCountry = reader["OriginCountry"].ToString(),
                                    Remaining = (long)reader["Remaining"]
                                });
                       }
                    }
                }
            }
            return real_flights;

        }

        // GET api/flights/5 flight by id
        public Flight Get(int id)
        {
            return flights.FirstOrDefault(_ => _.Id == id);
        }

        // POST api/flights -- add flight
        public void Post([FromBody] Flight flight)
        {
            flights.Add(flight);
        }

        // PUT api/flights/5 -- update flight
        public void Put(int id, [FromBody] Flight flight)
        {
            foreach (var item in flights)
            {
                if (item.Id == id)
                {
                    item.Id = flight.Id;
                    item.OriginCountry = flight.OriginCountry;
                    item.DestCountry = flight.DestCountry;
                    item.Remaining = flight.Remaining;
                    return;
                }
            }
        }

        // DELETE api/flights/5 -- delete flight
        public void Delete(int id)
        {
            flights.RemoveAll(_ => _.Id == id);
        }
    }
}
