﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication3.Models
{
    public class Flight
    {
        // +default ctor

        public long Id { get; set; }
        public string OriginCountry { get; set; }
        public string DestCountry { get; set; }
        public long Remaining { get; set; }

    }
}